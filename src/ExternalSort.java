import java.io.*;
import java.util.*;

/**
 * Created by ssdmitriev on 16.12.15.
 */
public class ExternalSort {
    private static final int MAX_SIZE = 32;
    private int numberOfInts;
    private Queue<String> queue = new ArrayDeque<>();
    private static final String INPUT = "resources/input";
    private static final String LOG_OUTPUT = "resources/log";
    private static int turn;
    private static int numberOfNumbers;
    private static PrintWriter log;


    public static void main(String[] args) {
        try {
            ExternalSort externalSort = new ExternalSort();
            for (turn = 0; turn < 10; turn ++) {
                log = new PrintWriter(new File(LOG_OUTPUT+turn));
                for (numberOfNumbers = 2; numberOfNumbers < 800000; numberOfNumbers += numberOfNumbers*2) {
                    createInput();
                    long startTime = System.nanoTime();
                    externalSort.sort(INPUT, "resources/output/");
                    long endTime = System.nanoTime();
                    long duration = (endTime - startTime);
//                    log.print(numberOfNumbers + " " + Double.valueOf(duration) / (numberOfNumbers * Math.log(numberOfNumbers)) + " ");
                    log.println(duration);
                }
                log.close();
            }

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void createInput() throws FileNotFoundException {
        PrintWriter out = new PrintWriter(new File(INPUT));
        log.println("input");
        Random rand = new Random();
        for (int j = 0; j < numberOfNumbers; j++) {
            int num = rand.nextInt();
            out.print(num);
            out.print(' ');
        }
        out.println();
        out.close();
    }

    public void sort(String inputFilePath, String outputDirPath) throws IOException {
//      делим исходный файл на несколько файлов размером с MAX_SIZE, сортируем каждый файл по отдельности
        firstStepMerge(inputFilePath, outputDirPath);
//      мержим файлы
        secondStepMerge(outputDirPath);
    }

    public void firstStepMerge(String inputFilePath, String outputFileTemplate) throws IOException {
        int i = 0;
        numberOfInts = 0;
        Scanner scannerX = new Scanner(new File(inputFilePath));
        int[] array = new int[MAX_SIZE];
        while (scannerX.hasNextInt()) {
            array[i] = scannerX.nextInt();
            if (i + 1 == MAX_SIZE || !scannerX.hasNextInt()) {
                String outputFilePath = outputFileTemplate + String.valueOf(numberOfInts);
                BufferedWriter outputWriter = new BufferedWriter(new FileWriter(outputFilePath));
                Arrays.sort(array, 0, i + 1);
                writeArrayToBuffer(array, i + 1, outputWriter);
                numberOfInts++;
                i = 0;
                queue.add(outputFilePath);
                outputWriter.close();
            }
            else {
                i++;
            }
        }
        scannerX.close();
    }

    public void secondStepMerge(String outputDirPath) throws IOException {
        int step = 0;
        while (queue.size() > 1) {
            int[] array = new int[MAX_SIZE];
            String inputFilePath1 = queue.poll();
            String inputFilePath2 = queue.poll();

            Scanner scanner1 = new Scanner(new File(inputFilePath1));
            Scanner scanner2 = new Scanner(new File(inputFilePath2));

            String outputFilePath = outputDirPath+ step + "_" + String.valueOf(numberOfInts);

            BufferedWriter outputWriter = new BufferedWriter(new FileWriter(outputFilePath));

            Integer firstNumber = null;
            Integer secondNumber = null;
            int i = 0;

            while ((scanner1.hasNextInt() || firstNumber != null) &&
                    (scanner2.hasNextInt() || secondNumber != null)) {
                firstNumber = (firstNumber == null) ? scanner1.nextInt() : firstNumber;
                secondNumber = (secondNumber == null) ? scanner2.nextInt() : secondNumber;
                if (firstNumber < secondNumber) {
                    array[i] = firstNumber;
                    firstNumber = null;
                }
                else {
                    array[i] = secondNumber;
                    secondNumber = null;
                }
                if (i + 1 == MAX_SIZE || !(scanner1.hasNextInt() || scanner2.hasNextInt())) {
                    writeArrayToBuffer(array, i + 1, outputWriter);
                    i = 0;
                }
                else {
                    i++;
                }
            }
            if (firstNumber != null) {
                array[0] = firstNumber;
            }
            if (secondNumber != null) {
                array[0] = secondNumber;
            }
            writeArrayToBuffer(array, 1, outputWriter);
            i = 0;
            while (scanner1.hasNextInt()) {
                array[i] = scanner1.nextInt();
                if (i + 1 >= MAX_SIZE || !scanner1.hasNextInt()) {
                    writeArrayToBuffer(array, i + 1, outputWriter);
                    i = 0;
                }
                else {
                    i++;
                }

            }
            while (scanner2.hasNextInt()) {
                array[i] = scanner2.nextInt();
                if (i + 1 == MAX_SIZE || !scanner2.hasNextInt()) {
                    writeArrayToBuffer(array, i + 1, outputWriter);
                    i = 0;
                }
                else {
                    i++;
                }
            }
            queue.add(outputFilePath);
            scanner1.close();
            File f1 = new File(inputFilePath1);
            f1.delete();
            scanner2.close();
            File f2 = new File(inputFilePath2);
            f2.delete();
            step++;
        }
        File f1 = new File(queue.poll());
        f1.renameTo(new File("resources/output/result" + String.valueOf(turn) + "_" + numberOfNumbers));
        
    }

    private void writeArrayToBuffer(int[] array, int size, BufferedWriter outputWriter) throws IOException {
        for (int i = 0; i < size; i++) {
            outputWriter.write(String.format("%d", array[i]));
            outputWriter.write(' ');
        }
        outputWriter.flush();
    }
}

